SLASH_SPITFIRE1 = "/spitfire"
SLASH_ICEBLOCK1 = "/iceblock"
SLASH_EVO1 = "/evocation"
SLASH_EVO2 = "/evo"
SLASH_MANAGEM1 = "/managem"
function truncateUntilInteger(a,b)
    return (a - a % b) / b
end
function secondsToDate(seconds)
	if seconds >= 3600 then
		local hour = truncateUntilInteger(seconds, 3600)
		local full_minutes = ( (seconds / 3600) - hour ) * 60
		local minutes = truncateUntilInteger(full_minutes, 60)
		local full_seconds = (full_minutes - minutes) * 60
		return (hour .. ":" .. string.format( "%02d", minutes ) .. ":" .. string.format( "%02d", full_seconds ))
	elseif seconds >= 60 then
		local full_minutes = seconds / 60
		local minutes = truncateUntilInteger(seconds, 60)
		local full_seconds = ( full_minutes - minutes ) * 60
		return ( string.format( "%02d", minutes ) .. ":" .. string.format( "%02d", full_seconds ) )
	else
		return string.format( "%02d", seconds )
	end
end
SlashCmdList["SPITFIRE"] = function(self, txt)
	local curseId = 42945
	if IsCurrentSpell(curseId) == 1 then
		SendChatMessage("{star} " .. GetUnitName("player") .. " used " .. GetSpellLink(curseId) .. "! {star}","RAID")
	else
		local usedAt, cd = GetSpellCooldown(curseId)
		local now = GetTime()
		local timeRemain = cd - (now - usedAt)
		
		if timeRemain > 1.5 then
			SendChatMessage("{star} " .. GetSpellLink(curseId) .. " will be ready within " .. secondsToDate(timeRemain) .. ". {star}","RAID")
		else
			SendChatMessage("{star} " .. GetUnitName("player") .. " used " .. GetSpellLink(curseid) .. "!","RAID")
		end
	end
end
SlashCmdList["ICEBLOCK"] = function(self, txt)
	local curseId = 45438
	if IsCurrentSpell(curseId) == 1 then
		SendChatMessage("{star} " .. GetUnitName("player") .. " used " .. GetSpellLink(curseId) .. "! {star}","RAID")
	else
		if (IsCurrentSpell(1953)) then return end -- 1953 = Blink
		
		local usedAt, cd = GetSpellCooldown(curseId)
		local now = GetTime()
		local timeRemain = cd - (now - usedAt)
		
		if timeRemain > 1.5 then
			SendChatMessage("{star} " .. GetSpellLink(curseId) .. " will be ready within " .. secondsToDate(timeRemain) .. ". {star}","RAID")
		else
			SendChatMessage("{star} " .. GetUnitName("player") .. " used " .. GetSpellLink(curseid) .. "!","RAID")
		end
	end
end
SlashCmdList["EVO"] = function(self, txt)
	local spellID = "Evocation"
	local spellUsed, spellCD, _ = GetSpellCooldown(spellID)
	local now = GetTime()
	local timeRemain = spellCD - (now - spellUsed)
	local currentSpellUsed = IsCurrentSpell(spellID)

	if timeRemain > 1.5 then
		SendChatMessage("{star} " .. GetSpellLink(spellID) .. " will be ready within " .. secondsToDate(timeRemain) .. ". {star}","RAID")
	elseif currentSpellUsed then
		SendChatMessage("{star} " .. GetUnitName("player") .. " used " .. GetSpellLink(spellID) .. "!","RAID")
	end
end
SlashCmdList["MANAGEM"] = function(self, txt)
	local id = 33312 -- mana gem
	local id2 = 42985 -- mana gem creator spell
	local usedAt, duration, _ = GetItemCooldown(id)
	local name, linkable = GetItemInfo(id)
	local inRange = IsItemInRange(soulstoneID, "target")
	local targetName = GetUnitName("target")
	local selfName = GetUnitName("player")
	local now = GetTime()
	local remainingTime = duration - (now - usedAt)
	local name = "Conjure Mana Gem"
	local currentItem = IsCurrentItem(id)
	
	if IsCurrentSpell(name) and GetItemCount(id, false, true) == 3 then
		SendChatMessage("{star} YOU can't create more " .. linkable .. " because it has full charge " .. selfName .. "!" , "RAID")
		return
	elseif IsCurrentSpell(name) and GetItemCount(id, false, true) < 3 then
		SendChatMessage("{star} " .. selfName .. " is creating " .. linkable .. "! {star}" , "RAID")
		return
	end

	--SendChatMessage("used: " .. usedAt,"RAID")
	--SendChatMessage("now: " .. now,"RAID")
	--SendChatMessage("time elapsed: " .. remainingTime,"RAID")
	--SendChatMessage("duration: " .. duration,"RAID")
	--print(name)
	--print(remainingTime)
	--print(linkable)
	--print(secondsToDate(60))
	
	if not (usedAt == 0) then
		SendChatMessage("{star} " .. linkable .. " will be ready within " .. secondsToDate(remainingTime) .. ". {star}","RAID")
	else
		if GetItemCount(id, false, true) == 0 and not (IsMounted() == 1) then
			SendChatMessage("{star} " .. selfName .. " has no more " .. linkable .. ". Create it dumb-ass!!! {star}", "RAID")
		elseif currentItem then
			SendChatMessage("{star} " .. selfName .. " used " .. linkable .. "!", "RAID")
		end
	end
end